function aVeryBigSum(ar) {
  // Write your code here
  let sum = 0;
  let temp;
  for(let i = 0; i < ar.length; i++) {
      temp = ar[i];
      sum += temp;
  }
  return sum;
}
