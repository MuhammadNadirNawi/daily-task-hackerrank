# Chapter 3 Daily Task - HackerRank

visit to [HackerRank](https://www.hackerrank.com/domains/algorithms).

- Copy function [`SolveMeFirst.js`](./SolveMeFirst.js) to complete function [Solve Me First](https://www.hackerrank.com/challenges/solve-me-first/problem?isFullScreen=true).
- Copy function [`SimpleArraySum.js`](./SimpleArraySum.js) to complete function [Simple Array Sum](https://www.hackerrank.com/challenges/simple-array-sum/problem?isFullScreen=true).
- Copy function [`ComparetheTriplets.js`](./ComparetheTriplets.js) to complete function [Compare the Triplets](https://www.hackerrank.com/challenges/compare-the-triplets/problem?isFullScreen=true).
- Copy function [`AVerryBigSum.js`](./AVerryBigSum.js) to complete function [A Verry Big Sum](https://www.hackerrank.com/challenges/a-very-big-sum/problem?isFullScreen=true).
- Copy function [`PlusMinus.js`](./PlusMinus.js) to complete function [Plus Minus](https://www.hackerrank.com/challenges/plus-minus/problem?isFullScreen=true).
- Copy function [`ArrayDS.js`](./ArrayDS.js) to complete function [Array DS](https://www.hackerrank.com/challenges/arrays-ds/problem?isFullScreen=true).
