function compareTriplets(a, b) {
  let alicePoint = 0;
  let bobPoint = 0;
     for(let i = 0; i < 3; i++) {
          if(a[i] > b[i]){
         alicePoint += 1;
     } else if (a[i] < b[i]) {
         bobPoint += 1;
     }
  }
  let result = [alicePoint, bobPoint];
  return result;
}
