function simpleArraySum(ar) {
  // Write your code here
  let sum = 0;
  let number;
  for(let i = 0; i < ar.length; i++) {
      number = ar[i];
      sum += number;
  }
  return sum;
}